This page is to display current perks that are build into a donor(+)'s rank on the server!

### Donor

Any player that has made at least one $5 contribution to Spinalcraft
* Slip: 3 slip endpoints by default
* Fly: 20 minutes of /fly by default each day


### Donor+

Any player that makes subscribes to a monthly contribution of $5 or more to Spinalcraft

In the event that the monthly contribution ends, cancels, or declines, the player will retain their Donor+ status thru the end of the last month paid.

* Slip: 4 slip endpoints by default
* Fly: 50 minutes of /fly by default each day


To comply with Mojang's EULA, these perks are limited to items that ALL player's have access to at this time, but in higher amounts. Staff is committed to expanding these benefits, so please checkback for information. We are also open to any [suggestions](https://dyno.gg/form/e78f0698) you may have!