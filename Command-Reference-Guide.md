A guide to help player's with commands on the server!




| **Command** | Alias|  **Description** |
| -------- | -------- | -------- |
| /help | - | Display's an ingame help menu for finding information |
| /spawn | /lobby,/tpa,/hub| Open's in-game gui for teleporting to places of interest |
| /fly | - | Allows the player to fly for a duration of time. Time is accrued from voting. Time will be displayed in bossbar at top of screen | 
| /fly bottom | - | Moves the fly timer to the action bar. Does not persist |
| /vote | - | Opens in-game menu to view vote sites, information, links, and a vote streak leaderboard |
| /voteshop | - | Opens in-game gui to view Vote Shop, where a player can spend their Vote Coins they have accrued from voting |
| /report | - | Opens in-game menu to Spinalcraft's [Issue Report](https://dyno.gg/form/9bb0142b) and [Staff Report](https://dyno.gg/form/fa86703c) |
| /slip | - | Display's information about Slipdisk and how to use
| /shopkeeper | - | Spinalcraft's internal manager for managing a shopkeeper that is compatible with the Market's plugin |
| /ecs create | - | Usage: /ecs create BuyPrice SellPrice - Hold item you want to sell, type command, and click chest to create a bulk sell chest: [Tutorial](https://www.youtube.com/watch?v=kwAz2Syk-y0&ab_channel=ServerMiner) |
| /taxreturn | - | Silly commnand to claim a penny on server |
| /maxhealth amount| - | Donor and above only. Allows player to set difficulty level by adjusting max health between 1-20|




