# Meet our staff
## **Moderators**
> Moderators help support our server by ensuring that all players follow our rules, and are our first line of staff to assist players in their day to day Spinalcraft experience! This level of our staff typically has very few permissions above a normal player, and will be able to quickly respond to chat and player issues, for example as temp-banning/muting, investigating griefing reports, etc.
### **Loz_Link**
> 	PLACEHOLDER
### **Sadiegamermom**
> PLACEHOLDER
### **ItsJayp**
> Placeholder

## **Officers**
> Our officers are experienced staff members with elevated privileges, able to assist players in a more elevated fashion, such as responding to griefing reports, full range of game mode access, and have been trusted with making some of the harder decisions such as permanent bans. Each of our officers does have a niche, making them a well rounded team!

### **Sagiplus**
*Community Engagement Officer*
> Sagiplus specializes in engaging with our community as a whole, creating a family friendly and fun environment on the  server by welcoming and assisting our players with many of their needs!

	 "PLACEHOLDER"

### **Lightgazer**
*Content Creator Engagement Officer*
> Lightgazer specializes in engaging and assisting our Content Creators and communities surrounding them! 

> "Hello I'm Lightgazer, the guy who really wants to help make Spinalcraft a friendly environment for everyone! If you need a hand, let me know!"

### Its_Lucid_
*Creative and Events Officer*
> Its_Lucid_ specializes in specialized world edit and building, along with assisting with design of Spinalcraft's events!

	 "PLACEHOLDER"

## Technical Specialist
### Belinski20
> Our technical specialist assists with responding to technical bugs and needs of the Spinalcraft server behind the scenes! While not as actively engaged with the community on a moderation level, Belinski provides a valuable resource in maintaining some of our unique plugins and assisting with error management! 

	 "After getting a suggestion to join SpinalCraft back in 2014 from fellow players, I joined and played on it until burning out on Survival and Modded. Still wanting to be an active member of the community and to keep the existing unique plugins alive I swapped to a more passive role of working on plugin development and maintenance; to keep giving SpinalCraft a more unique feel over just vanilla servers for the versions to come"

## Server Administrator
### Jason246
> Our server administrator actively engages with the community and rest of staff, ensuring that appropriate moderation is occurring and responding to all inquiries that require group input. Jason also helps with making final decisions on the path the server should take

	 "I'm Jasooon, I've been on Spinalcraft since 2015, and I'm usually the one who breaks the server! I'm here to deal with any type of issue and I'm more than happy to hear anyone out."

## Owner
### GeneralX

> The owner of the server! Actively develops the server, including event technical work, server maintenance such as updates, new feature development, etc, and oversees and make's the final decisions when involdved with moderation of players!

	"Originally joining Spinalcraft back in 2015 just as a Spinaling after just randomly coming across the first multiplayer server I could find, I have been apart of this community for almost a decade! After becoming a staff member, eventually the server came to a point where it needed help being restored and rebooted, and I stepped up to the job in August of 2017 to take on the role of maintaining the server and community! It has been a true blessing to get to know all of the great folks that play, and watch this community prosper and maintain! Thank you for each of you that continue to be apart of the Spinalcraft Community!"
