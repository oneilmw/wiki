#  Thank you for considering to support Spinalcraft!
 
 It is with great pleasure that we announce we are accepting donations for Spinalcraft! Your donations will help us maintain ongoing costs for remote hosting, providing smooth gameplay for all users, extend the longevity of our worlds, and be able to keep providing new features to players as Minecraft continues to evolve and change!

Spinalcraft has been in operation since late 2013, and our community consists of some of the most well connected players in the Minecraft community, with many of us meeting and becoming aquatinted over the last 10 years! We certainly have had a lot of fun building, creating, and destroying together, and are hoping to continue this on for many years to come!

**Your donations go towards:**  

* Keeping our server well maintained and up to date with the latest technology
* Adding new features and content as needed Hosting new servers, such as Seasons, for modded content support
* Ensuring that our server worlds live as long as possible!
* We are ever grateful for your support you provide, and would hope that you consider donating today!

We are ever grateful for your support you provide, and would hope that you consider donating today!

To donate to Spinalcraft regularly, please visit our patreon by clicking below!: 

[PATREON](https://www.patreon.com/bePatron?u=23231330)

For a 1 time donation, you may use the following:
* Venmo: @GenX_LLC
* CashApp: $GeneralXLLC