By voting for Spinalcraft, you help promote Spinalcraft and push us to the top of the server lists! By doing so, it helps us gain new members and keep our community alive!

When you vote on a site recognized by Spinalcraft [[Voting Sites]] two things happen:

* You gain Vote Coins for use in the [[Vote Shop]]!
* You gain a short period of fly for every vote!

-----
## Fly Rewards for Voting

> [[Command Reference Guide]]: /fly

Each day, during the daily server restart, each player will be reset back to their "default" fly time for the day

| Player Rank | Default Fly Amount | 
| -------- | -------- | 
| Normal | 0 Minutes | 
| Donor | 20 Minutes |
| Donor+ | 50 Minutes |
| Creator | 180 Minutes |

* When you vote on any 1 of the 5 sites, you will queue 1 minute of flight time.
* When you vote on all 5 sites, you will queue an extra 5 minutes of flight time.
* When you activate /fly, this queued flight time will be added to your current  fly timer. IE if you have Donor, you will have 30 minutes after voting on all sites
* Queued time resets during the daily server restart, so stacking time is not allowed.
* Maximum flight accrual from voting is 10 minutes.
-----
## Vote Coin Accrual
* Each Vote gains the player 1 Vote Coin.
* Voting on all 5 sites gains the player a bonus 5 Vote Coins.
* Voting for multiple days in the row gain the player an additional bonus according the chart below:
|Daily Streak | Bonus Amount |
| -------- | -------- |
| 1-5 Days | 4 |
| 6-10 Days | 5 |
| 11-15 Days | 6 |
| 16-20 Days | 7 |
|21-25 Days | 8 |
| 26-30 Days | 9 |
| 31 Days | 10 |

Each month, the server will also give Vote Coin bonus for the server meeting its Donation goal! Based on server operating costs, and received donations, we will determine the server bonus on the first of each month. This bonus is applied when voting on all 5 sites in a day!



| Donation Percent | Bonus Amount |
| -------- | -------- |
| 1-10% | 1 |
| 11-20% | 1 |
| 21-30% | 1 |
| 31-40% | 1 |
| 41-50% | 1 |
| 51-60% | 1 |
| 61-70% | 1 |
| 71-80% | 1 |
| 81-90% | 1 |
| 91-100% | 1 |
| Total Possible Bonus | 10 |

When does each earning/bonus trigger?

	Each site will pay its Vote Coin out at each Vote

Bonus Trigger

	When you vote on all 5 sites, the bonus's will all trigger. 

	All sites bonus+streak bonus+ server donation goal bonus

	Maximum possible accrual per day is 30 Vote Coins




